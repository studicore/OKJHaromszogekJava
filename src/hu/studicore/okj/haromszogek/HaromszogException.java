/*
 * A program szabadon felhasználható - a szerzőre vonatkozó információk megváltoztatása nem megengedett!
 */
package hu.studicore.okj.haromszogek;

/**
 *
 * @author Pasztuhov Dániel, StudiCore Kft.
 * www.studicore.hu
 */
public class HaromszogException extends Exception {

    public HaromszogException(String string) {
        super(string);
    }
    
}
