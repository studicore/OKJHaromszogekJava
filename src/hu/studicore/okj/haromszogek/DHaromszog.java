/*
 * A program szabadon felhasználható - a szerzőre vonatkozó információk megváltoztatása nem megengedett!
 */
package hu.studicore.okj.haromszogek;

import java.util.Scanner;

/**
 *
 * @author Pasztuhov Dániel, StudiCore Kft.
 * www.studicore.hu
 */
public class DHaromszog {
    private double aOldal;
    private double bOldal;
    private double cOldal;
    private int sorSzama;

    public DHaromszog(String sor, int sorSzama) throws HaromszogException {
        Scanner sc = new Scanner(sor);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();
        this.sorSzama = sorSzama;
        
        setA(a);
        setB(b);
        setC(c);
        
        if (!isEllMegszerkesztheto()) {
            throw new HaromszogException("A háromszöget nem lehet megszerkeszteni!");
        }
        if (!isEllNovekvoSorrend()) {
            throw new HaromszogException("Az adatok nincsenek növekvő sorrendben!");
        }
        if (!isEllDerekszogu()) {
            throw new HaromszogException("A háromszög nem derékszögű!");
        }
    }
    
    private boolean isEllDerekszogu() {
        return aOldal * aOldal + bOldal * bOldal == cOldal * cOldal;
    }
    
    private boolean isEllMegszerkesztheto() {
        return aOldal + bOldal > cOldal;
    }
    
    private boolean isEllNovekvoSorrend() {
        return aOldal <= bOldal && bOldal <= cOldal;
    }

    public final void setA(double a) throws HaromszogException {
        if (a <= 0)
            throw new HaromszogException("Az 'a' oldal nem lehet nulla vagy negatív!");
        this.aOldal = a;
    }
    
    public final void setB(double b) throws HaromszogException {
        if (b <= 0)
            throw new HaromszogException("A 'b' oldal nem lehet nulla vagy negatív!");
        this.bOldal = b;
    }
    
    public final void setC(double c) throws HaromszogException {
        if (c <= 0)
            throw new HaromszogException("A 'c' oldal nem lehet nulla vagy negatív!");
        this.cOldal = c;
    }

    public double getA() {
        return aOldal;
    }

    public double getB() {
        return bOldal;
    }

    public double getC() {
        return cOldal;
    }
    
    public double getKerulet() {
        return aOldal + bOldal + cOldal;
    }
    
    public double getTerulet() {
        return aOldal * bOldal / 2.0;
    }

    public int getSorSzama() {
        return sorSzama;
    }

    public void setSorSzama(int sorSzama) {
        this.sorSzama = sorSzama;
    }
    
    @Override
    public String toString() {
        //return String.format("%d. sor: a=%.2f b=%.2f c=%.2f", sorSzama, aOldal, bOldal, cOldal);
        return sorSzama + ". sor: a="+(aOldal+" b="+bOldal+" c="+cOldal).replace('.', ',');
    }
    
}
